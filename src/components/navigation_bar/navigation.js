import React from 'react'
import { Link } from 'gatsby'
import './navigation.scss'

export default () => (
  <nav role="navigation">
    <div className="navigation">
      <div className="profile-pict" />
      <div className="navigationItem-socket">
        <span className="navigationItem-item">
          <Link to="/" activeClassName="navigationItem-active">Curriculum Vitae</Link>
        </span>
        <span className="navigationItem-item">
          <Link to="/blogs/" activeClassName="navigationItem-active">Projects</Link>
        </span>
      </div>
    </div>
  </nav>
)
