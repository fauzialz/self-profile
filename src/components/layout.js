import React from 'react'
import { Link } from 'gatsby'
import base from './base.css'
import Container from './container'
import Navigation from './navigation_bar'

class Template extends React.Component {
  render() {
    const { location, children } = this.props
    // console.log(location)
    let header

    let rootPath = `/`
    if (typeof __PREFIX_PATHS__ !== `undefined` && __PREFIX_PATHS__) {
      rootPath = __PATH_PREFIX__ + `/`
      debug
    }

    return (
      <React.Fragment>
        <Navigation />
        <Container>
          {children}
        </Container>
      </React.Fragment>
    )
  }
}

export default Template