import React from 'react'
import './text_mark.scss'

export default ({ children }) => {
    return (
        <span className="mark-text">
            {children}
        </span>
    )
}