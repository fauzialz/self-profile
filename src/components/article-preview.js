import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import styles from './article-preview.module.css'
import './article-preview.scss'

export default ({ article }) => (
  <div className={styles.preview}>
    <Img alt="" fluid={article.heroImage.fluid} />
    <h3 className="previewTitle">
      <Link to={`/blogs/${article.slug}`}>{article.title}</Link>
    </h3>
    <small>{article.publishDate}</small>
    {/* <p
      dangerouslySetInnerHTML={{
        __html: article.description.childMarkdownRemark.html,
      }}
    /> */}
    <p>
    {article.description.childMarkdownRemark.html}
    </p>
    {article.tags.map(tag => (
      <p className="tag" key={tag}>
        {tag}
      </p>
    ))}
  </div>
)
