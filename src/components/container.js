import React from 'react'
import './container.scss'

export default ({ children }) => (
  <div style={{ margin: '0 auto' }}>{children}</div>
)
