import React from 'react'
import './tooltip.scss'

export default ({ children, text, active }) => {
    return (
        <div className="tooltip-base">
            <div className={ active ? "tooltip-banner-active" : "tooltip-banner"}>
                {text}
            </div>
            { children }
        </div>
    )
}