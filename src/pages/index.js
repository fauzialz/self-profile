import React from 'react'
import { graphql } from 'gatsby'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import Hero from '../components/hero'
import Layout from '../components/layout'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import './index.scss'
import ArticlePreview from '../components/article-preview'
import Tooltip from '../components/tooltip';
import TextMark from '../components/text_mark';

library.add(fab, fas)

class RootIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    const posts = get(this, 'props.data.allContentfulBlogPost.edges')
    const [author] = get(this, 'props.data.allContentfulPerson.edges')
    const sidebar = [ 'Short Bio.', 'Study', 'Experiences']

    return (
      <Layout location={this.props.location} >
        <Helmet>
          <title>CV | Short Bio.</title>
          <link rel="icon" type="image/png" href="../../static/favicon.ico" sizes="16x16" />
        </Helmet>
        <div className="cv-socket">
          <div className="cv-sidebar-socket">

            {sidebar.map( (e,n) => (
              <Tooltip text={e} active={n == 0} key={n}>
                <div className={n == (sidebar.length - 1)? "sidebar-item last" : "sidebar-item"}>
                  <div className={n == 0? "sidebar-item-dot active" : "sidebar-item-dot"} />
                </div>
              </Tooltip>
            ))}
            
          </div>
          <div className="cv-content-socket">
            <div id="about-me">
              <div className="greating">
                Hey. My name is <TextMark>{author.node.name}</TextMark>, 
                <br />I am a <TextMark>{author.node.title}</TextMark>.
              </div>
              <div className="description">
                {author.node.shortBio.shortBio}
              </div>
            </div>
          </div>
        </div>
        {/* <div style={{ background: '#fff' }}>
          <Helmet title={siteTitle} />
          <Hero data={author.node} />
          <div className="wrapper">
            <h2 className="section-headline">Recent articless</h2>
            <ul className="article-list">
              {posts.map(({ node }) => {
                return (
                  <li key={node.slug}>
                    <ArticlePreview article={node} />
                  </li>
                )
              })}
            </ul>
          </div>
        </div> */}
      </Layout>
    )
  }
}

export default RootIndex

export const pageQuery = graphql`
  query HomeQuery {
    site {
      siteMetadata {
        title
      }
    }
    allContentfulBlogPost(sort: { fields: [publishDate], order: DESC }) {
      edges {
        node {
          title
          slug
          publishDate(formatString: "MMMM Do, YYYY")
          tags
          heroImage {
            fluid(maxWidth: 350, maxHeight: 196, resizingBehavior: SCALE) {
             ...GatsbyContentfulFluid_tracedSVG
            }
          }
          description {
            childMarkdownRemark {
              html
            }
          }
        }
      }
    }
    allContentfulPerson(filter: { contentful_id: { eq: "15jwOBqpxqSAOy2eOO4S0m" } }) {
      edges {
        node {
          name
          shortBio {
            shortBio
          }
          title
          heroImage: image {
            fluid(
              maxWidth: 1180
              maxHeight: 480
              resizingBehavior: PAD
              background: "rgb:000000"
            ) {
              ...GatsbyContentfulFluid_tracedSVG
            }
          }
        }
      }
    }
  }
`
